﻿using Modelos;
using Modelos.Interfaces;
using Modelos.Modelos;
using Modelos.Repositórios;
using System;

namespace RequerenteRotina
{
    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            var ambienteAlvo = EnvironmentVariableTarget.Machine;
#endif
#if DOCKER
            var ambienteAlvo = EnvironmentVariableTarget.Process;
#endif

            var tempoRequisiçãoAmbiente = Environment.GetEnvironmentVariable("TempoRequisicao", ambienteAlvo);
            var tempoRequisição = Int32.Parse(tempoRequisiçãoAmbiente);

            while (true)
            {
                var momento = DateTime.Now.AddMinutes(tempoRequisição);
                while (DateTime.Now < momento)
                {
                    //Esperar
                }

                using (var contexto = new AplicaçãoContext())
                {
                    var repositório = new RequisiçõesRepositório(contexto);
                    var instante = DateTime.Now;
                    var item = new RequisiçãoModelo() { Momento = instante };
                    repositório.InserirUm(item);

                    Console.WriteLine($"Requisição inserida com sucesso! Momento: {instante}");
                }
            }
        }
    }
}
